--- 
title: "ISLR2 Labs (as a book)"
author: "DAT Linux"
date: "2022-08-31"
site: bookdown::bookdown_site
documentclass: book
bibliography: [book.bib, packages.bib]
description: |
  This is bookdown book of the labs for the book: An Introduction to 
  Statistical Learning with Applications in R - 2ed.
biblio-style: apalike
csl: chicago-fullnote-bibliography.csl
---

# Welcome

<img src="cover.png" />

This is an HTML `bookdown` book of the code labs for **An Introduction to Statistical Learning with Applications in R - 2ed. Springer, 2021** (aka **ISLR2**), which have been run and converted to a handy HTML output format for each chapter.

## Rmd files 

See the Git repo for each page's corresponding R Markdown file. 

## Brought to you by..

The team at [DAT Linux](http://datlinux.com) - the data science OS.



