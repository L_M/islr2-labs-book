https://l_m.gitlab.io/islr2-labs-book

This is an HTML bookdown book of the labs for **An Introduction to Statistical Learning with Applications in R - 2ed. Springer, 2021** (aka **ISLR2**), which have been run and converted to a handy HTML output format for each chapter.
